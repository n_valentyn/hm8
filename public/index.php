<?php
include_once '../vendor/autoload.php';

use App\Color;

try {
    $errorColor = new Color(255, 0, -1);
    //$errorColor = new Color(240,256,10);
} catch (\InvalidArgumentException $e) {
    echo $message = $e->getMessage() . '<br>';
}

$color = new Color(200, 200, 200);
$mixedColor = $color->mix(new Color(100, 100, 100));
$red = $mixedColor->getRed(); // 150
$green = $mixedColor->getGreen(); // 150
$blue = $mixedColor->getBlue(); // 150

$random1 = Color::random();
$random2 = Color::random();

$dont_equals = $random1->equals($random2);

$equals = $mixedColor->equals(new Color(150, 150, 150));

echo $equals;




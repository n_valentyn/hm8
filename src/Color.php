<?php

namespace App;

class Color
{
    private int $red;
    private int $green;
    private int $blue;

    /**
     * Color constructor.
     * @param int $red
     * @param int $green
     * @param int $blue
     */
    public function __construct(int $red, int $green, int $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    /**
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @param int $red
     */
    private function setRed(int $red): void
    {
        if (!self::isDecimalCode($red)) {
            throw new \InvalidArgumentException('Red color is invalid');
        }
        $this->red = $red;
    }

    /**
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @param int $green
     */
    private function setGreen(int $green): void
    {
        if (!self::isDecimalCode($green)) {
            throw new \InvalidArgumentException('Green color is invalid');
        }
        $this->green = $green;
    }

    /**
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @param int $blue
     */
    private function setBlue(int $blue): void
    {
        if (!self::isDecimalCode($blue)) {
            throw new \InvalidArgumentException('Blue color is invalid');
        }
        $this->blue = $blue;
    }

    /**
     * Validating the decimal color code
     * @param int $code
     * @return bool
     */
    public static function isDecimalCode(int $code): bool
    {
        if ($code < 0 || $code > 255) {
            return false;
        }
        return true;
    }

    /**
     * Compare with another color
     * @param Color $color
     * @return bool
     */
    public function equals(Color $color): bool
    {
        if ($this == $color) {
            return true;
        }
        return false;
    }

    /**
     * Random color generation
     * @return Color
     */
    public static function random(): Color
    {
        return new Color(random_int(0, 255), random_int(0, 255), random_int(0, 255));
    }

    /**
     * Mix with another color
     * @param Color $color
     * @return Color
     */
    public function mix(Color $color): Color
    {
        return new Color (
            (int)(($this->getRed() + $color->getRed()) / 2),
            (int)(($this->getGreen() + $color->getGreen()) / 2),
            (int)(($this->getBlue() + $color->getBlue()) / 2)
        );
    }
}